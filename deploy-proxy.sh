#!/usr/bin/env sh

# clean deployment of proxy, gen, and ssl containers
# $1 = user (must have authorized ssh key)
# $2 = remote docker server

# vars
PROXY_CONTAINER='NGINX'
PROXY_IMAGE='nginx:alpine'
PROXY_GEN_CONTAINER='NGINX-Gen'
PROXY_GEN_IMAGE='jwilder/docker-gen'
LETSENCRYPT_CONTAINER='LetsEncrypt'
LETSENCRYPT_IMAGE='jrcs/letsencrypt-nginx-proxy-companion'

# make required directories
ssh -o StrictHostKeyChecking=no -T $1@$2 << ENDSSH
    sudo mkdir -p /var/templates
    sudo chown $1:$1 /var/templates
    sudo mkdir -p /var/vhost.d/
    sudo chown $1:$1 /var/vhost.d
ENDSSH

# deploy latest nginx docker-gen template
TMP=./.tmp
mkdir -p $TMP
curl https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl > $TMP/nginx.tmpl
scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -q -r $TMP/nginx.tmpl $1@$2:/var/templates/nginx.tmpl
rm -rf $TMP

# replace docker containers
ssh -o StrictHostKeyChecking=no -T $1@$2 << ENDSSH
    docker stop $PROXY_CONTAINER
    docker rm $PROXY_CONTAINER
    docker pull $PROXY_IMAGE
    docker run -d \
        -p 80:80 -p 443:443 \
        --name $PROXY_CONTAINER \
        --restart=unless-stopped \
        -v /etc/nginx/conf.d \
        -v /var/vhost.d:/etc/nginx/vhost.d:ro \
        -v /usr/share/nginx/html \
        -v /var/certs:/etc/nginx/certs:ro \
        $PROXY_IMAGE
    docker stop $PROXY_GEN_CONTAINER
    docker rm $PROXY_GEN_CONTAINER
    docker pull $PROXY_GEN_IMAGE
    docker run -d \
        --name $PROXY_GEN_CONTAINER \
        --restart=unless-stopped \
        --volumes-from $PROXY_CONTAINER \
        -v /var/templates:/etc/docker-gen/templates:ro \
        -v /var/run/docker.sock:/tmp/docker.sock:ro \
        $PROXY_GEN_IMAGE \
        -notify-sighup $PROXY_CONTAINER -watch -wait 5s:30s /etc/docker-gen/templates/nginx.tmpl /etc/nginx/conf.d/default.conf
    docker stop $LETSENCRYPT_CONTAINER
    docker rm $LETSENCRYPT_CONTAINER
    docker pull $LETSENCRYPT_IMAGE
    docker run -d \
        --name $LETSENCRYPT_CONTAINER \
        --restart=unless-stopped \
        --volumes-from $PROXY_CONTAINER \
        -v /var/certs:/etc/nginx/certs:rw \
        -v /var/vhost.d:/etc/nginx/vhost.d:rw \
        -v /var/run/docker.sock:/var/run/docker.sock:ro \
        -e "NGINX_PROXY_CONTAINER=$PROXY_CONTAINER" \
        -e "NGINX_DOCKER_GEN_CONTAINER=$PROXY_GEN_CONTAINER" \
        $LETSENCRYPT_IMAGE
ENDSSH

