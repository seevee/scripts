# clean restart of dns container

# must be able to key auth to server

# docker vars
DNS_CONTAINER='BIND'
DNS_IMAGE='sameersbn/bind:latest'
DNS_VOL='bind-dns-data'

# ssh command
ssh -o StrictHostKeyChecking=no -T $1 << ENDSSH
    docker stop $DNS_CONTAINER
    docker rm $DNS_CONTAINER
    docker pull $DNS_IMAGE
    docker run -d \
        --name $DNS_CONTAINER \
        --publish 53:53/tcp \
        --publish 53:53/udp \
        --publish 10000:10000/tcp \
        --restart=unless-stopped \
        -v $DNS_VOL:/data \
        $DNS_IMAGE
ENDSSH
